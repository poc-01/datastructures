package ds.array;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class SimpleArray {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arr[] = new int[3];
        System.out.println("Enter element: ");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println("Print element: ");
        //For each loop
        // for (int ele : arr) {
        //    System.out.println(ele);
        // }
        System.out.println(Arrays.toString(arr));  // Arrays class
    }
}
