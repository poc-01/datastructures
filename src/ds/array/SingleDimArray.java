package ds.array;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class SingleDimArray {
    public static void main(String[] args) {
        int[] marks = new int[5];
        //int[] marks = new int[]{55,66,87,45,34};  // initialising with data
        //Default array data
        System.out.println("marks[0] : " + marks[0]);
        System.out.println("marks[1] : " + marks[1]);
        System.out.println("marks[2] : " + marks[2]);
        System.out.println("marks[3] : " + marks[3]);
        System.out.println("marks[4] : " + marks[4]);
        //Adding array data
        marks[0] = 80;
        marks[1] = 70;
        marks[2] = 88;
        marks[3] = 98;
        marks[4] = 55;
        //After adding data
        System.out.println("Reading data individual");
        System.out.println("marks[0] : " + marks[0]);
        System.out.println("marks[1] : " + marks[1]);
        System.out.println("marks[2] : " + marks[2]);
        System.out.println("marks[3] : " + marks[3]);
        System.out.println("marks[4] : " + marks[4]);
        System.out.println("Array length : " + marks.length);

        //Reading data using for loop
        System.out.println("Reading data using for loop");
        for (int i = 0; i <= marks.length - 1; i++) {
            System.out.println(marks[i]);
        }

        //Reading data using for enhanced for loop
        System.out.println("Reading data using enhanced for loop");
        for (int ele : marks) {
            System.out.println(ele);
        }

    }


}
