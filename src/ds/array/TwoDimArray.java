package ds.array;

public class TwoDimArray {
    public static void main(String[] args) {
        //1st  & 2nd sem marks
        int[][] semMarks = new int[][]{
                {45, 34, 54, 32, 44}, {66, 77, 55, 44, 43}
        };
        System.out.println("Reading data - for loop");
        for (int i = 0; i <= semMarks.length - 1; i++) {
            for (int j = 0; j <= semMarks[i].length - 1; j++) {
                System.out.println(semMarks[i][j]);
            }
        }
    }
}
