package ds.queue;

import java.util.LinkedList;
import java.util.Queue;

public class QueueClass {
    public static void main(String[] args) {


        Queue<Integer> q = new LinkedList<>();

        for(int i=0;i<5;i++)
        {
            q.add(i);
        }
        System.out.println("Queue element: " +q);

        //Remove queue element

        int removeele = q.remove();
        System.out.println("remove element : " +removeele);
        System.out.println("Queue element: " +q);
        System.out.println("Queue size: " +q.size());


        int removeele1 = q.remove();
        System.out.println("remove element : " +removeele1);
        System.out.println("Queue element: " +q);
        System.out.println("Queue size: " +q.size());

    }
}
