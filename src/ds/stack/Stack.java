package ds.stack;

public class Stack {
    static final int MAX = 5;
    int[] a = new int[MAX];
    int top;

    Stack() {
        top = -1;
    }

    boolean isEmpty() {
        return (top < 0);

    }

    boolean push(int x) {
        if (top >= MAX - 1) {
            System.out.println("stack overflow");
            return false;
        } else {

            a[++top] = x;
            System.out.println(x + " Pushed into stack");
            return true;
        }
    }

    int pop() {
        if (top < 0) {
            System.out.println("Stack underflow");
            return 0;
        } else {

            int x = a[top--];

            return x;
        }
    }

    public static void main(String[] args) {
        Stack stack = new Stack();
        stack.push(10);
        stack.push(20);
        stack.push(30);
        stack.push(45);
        stack.push(85);
        System.out.println("poped item : " + stack.pop());
        System.out.println("poped item : " + stack.pop());
        System.out.println("poped item : " + stack.pop());
        System.out.println("poped item : " + stack.pop());
        System.out.println("poped item : " + stack.pop());
        //  System.out.println("poped item : " + stack.pop());
    }
}
